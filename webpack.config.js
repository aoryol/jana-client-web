const webpack = require('webpack');
const CleanPlugin = require('clean-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssPlugin = require('optimize-css-assets-webpack-plugin');

function config(fn) {
  return (env, argv) =>
    fn(Object.assign({}, env, {
      prod: argv.mode === 'production',
      dev: argv.mode === 'development'
    }));
}

module.exports = config(env => ({
  entry: {
    main: ['babel-polyfill', './src/index.js'],
    editor: ['babel-polyfill', './src/components/editor/index.js']
  },
  resolve: {
    extensions: ['*', '.js', '.vue']
  },
  devtool: env.prod ? undefined : 'eval-source-map',
  module: {
    rules: [
      {
        test: /\.html$/,
        use: {
          loader: 'html-loader',
          options: { minimize: true }
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: { loader: 'babel-loader' }
      },
      {
        test: /\.(s?)css$/,
        use: [
          { loader: env.prod ? MiniCssExtractPlugin.loader : 'vue-style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' }
        ]
      },
      {
        test: /\.vue$/,
        use: { loader: 'vue-loader' }
      },
      {
        test: /\.(woff|ttf)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 50000,
            name: './dist/fonts/[name].[ext]'
          }
        }
      }
    ]
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: true
      }),
      new OptimizeCssPlugin({})
    ]
  },
  plugins: [
    new CleanPlugin(['dist']),
    new HtmlPlugin({
      template: './src/index.html',
      chunks: ['main']
    }),
    new HtmlPlugin({
      filename: 'auth-redirect.html',
      template: './src/auth-redirect.html',
      inject: false
    }),
    new HtmlPlugin({
      filename: 'editor.html',
      template: './src/components/editor/index.html',
      chunks: ['editor']
    }),
    new webpack.EnvironmentPlugin({
      JANA_API_URL: env.api_url || '/',
      USE_APP_CACHE: (env.use_app_cache || env.prod) ? 'true' : ''
    }),
    new MiniCssExtractPlugin({
      filename: 'styles-[name].css'
    }),
    new WorkboxPlugin.GenerateSW({
      skipWaiting: true,
      clientsClaim: true
    })
  ],
  stats: env.prod ? 'normal' : 'minimal',
  devServer: {
    stats: 'minimal'
  }
}));
