const path = require('path');

module.exports = {
  extends: [
    'airbnb-base',
    'plugin:vue/recommended',
    'plugin:jest/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2018
  },
  plugins: [
    'vue',
    'jest'
  ],
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['~', path.resolve(__dirname, './src')]
        ],
        extensions: [
          '.js',
          '.vue'
        ]
      }
    }
  },
  rules: {
    'comma-dangle': [
      'error',
      'never'
    ],
    'import/extensions': [
      'error',
      'never'
    ],
    'no-param-reassign': [
      'error',
      {
        props: false
      }
    ],
    'no-restricted-syntax': [
      'error',
      'LabeledStatement',
      'WithStatement'
    ],
    'no-underscore-dangle': [
      'error',
      {
        allowAfterThis: true
      }
    ],
    'no-use-before-define': [
      'error',
      'nofunc'
    ],
    'operator-linebreak': [
      'error',
      'after'
    ],
    'prefer-destructuring': 'off',
    'vue/html-closing-bracket-newline': [
      'error',
      { singleline: 'never', multiline: 'never' }
    ],
    'vue/html-indent': [
      'error',
      2,
      {
        alignAttributesVertically: false
      }
    ],
    'vue/max-attributes-per-line': [
      'error',
      {
        singleline: 4,
        multiline: {
          max: 3,
          allowFirstLine: true
        }
      }
    ]
  }
};
