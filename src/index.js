import axios from 'axios';

import Vue from 'vue';
import VueRouter from 'vue-router';
import Element from 'element-ui';

import 'element-ui/lib/theme-chalk/reset.css';
import 'element-ui/lib/theme-chalk/index.css';

import store from './store';
import routes from './routes';

import './components/common';
import App from './components/app';

Vue.use(VueRouter);
Vue.use(Element, { size: 'small' });

if (navigator.serviceWorker && !!process.env.USE_APP_CACHE) {
  navigator.serviceWorker.register('/service-worker.js');
}

axios.defaults.baseURL = process.env.JANA_API_URL;

(async function run() {
  await store.dispatch('init');
  const router = new VueRouter({ routes });

  // eslint-disable-next-line no-new
  new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
  });
}());
