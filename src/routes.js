import * as views from './components/router-views';

export default [
  {
    path: '/',
    redirect: '/notes'
  },
  {
    path: '/notes',
    components: {
      default: views.NoteListPage,
      toolbar: views.NoteListToolbar
    },
    meta: { auth: true }
  },
  {
    path: '/notes/new',
    components: {
      default: views.NoteEditPage,
      toolbar: views.NoteEditToolbar
    },
    meta: { auth: true }
  },
  {
    path: '/notes/edit/:id',
    components: {
      default: views.NoteEditPage,
      toolbar: views.NoteEditToolbar
    },
    props: {
      default: route => ({ noteId: route.params.id })
    },
    meta: { auth: true }
  },
  {
    path: '/notes/:publicName',
    components: {
      default: views.NoteViewPage
    },
    props: {
      default: route => ({ notePublicName: route.params.publicName })
    },
    meta: {
      toolbar: false
    }
  }
];
