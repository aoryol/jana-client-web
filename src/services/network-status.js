export default {
  isOffline() {
    return !navigator.onLine;
  }
};
