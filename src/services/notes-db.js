import { set as idbSet, get as idbGet } from 'idb-keyval';

const KEYS = {
  notes: userId => `jana:notes:${userId}`,
  modifications: userId => `jana:modifications:${userId}`
};

const getter = collection => async userId => (
  await idbGet(KEYS[collection](userId)) || []
);

const setter = collection => async (userId, value) => (
  idbSet(KEYS[collection](userId), value || [])
);

export default {
  getNotes: getter('notes'),
  setNotes: setter('notes'),

  getModifications: getter('modifications'),
  setModifications: setter('modifications')
};
