import _ from 'lodash';
import { ObjectId } from 'bson/lib/bson/objectid';

import AuthApi from '../api/auth-api';
import NotesApi from '../api/notes-api';
import NetworkStatus from '../services/network-status';
import NotesDb from '../services/notes-db';

import * as GlobalActions from './global-actions';

const MUT_RESET = 'reset';
const MUT_SET_NOTES = 'setNotes';
const MUT_ADD_MODIFICATIONS = 'addModifications';
const MUT_CLEAR_MODIFICATIONS = 'clearModifications';
const MUT_APPLY_UPDATES = 'applyUpdates';
const MUT_SET_TIMER = 'setTimer';
const MUT_SET_LAST_SYNC_STATUS = 'setLastSyncStatus';
const MUT_UPDATE_LAST_SYNC_TIMESTAMP = 'updateLastSyncTimestamp';
const MUT_SET_CURRENT_NOTE = 'setCurrentNote';

const ACT_LOAD_DB = 'loadDb';
const ACT_SAVE_DB = 'saveDb';
const ACT_SYNC = 'sync';
const ACT_CLEAR_SYNC_TIMER = 'clearSyncTimer';
const ACT_SYNC_TIMER = 'syncTimer';
const ACT_OPEN = 'open';
const ACT_CLOSE = 'close';
const ACT_MODIFY = 'modify';
const ACT_ADD = 'add';
const ACT_UPDATE = 'update';
const ACT_REMOVE = 'remove';
const ACT_PUBLISH = 'publish';
const ACT_UNPUBLISH = 'unpublish';

const TIMER_INTERVAL = {
  normal: 30 * 60,
  readFail: 3 * 60,
  writeFail: 30
};

function applyModifications(notes, modifications, modificationTypeFn) {
  if (_.isEmpty(modifications)) {
    return notes;
  }

  const modificationsByType = _.groupBy(modifications, modificationTypeFn);
  const deleted = new Set(_.map(modificationsByType.remove, 'id'));
  const added = modificationsByType.add || [];
  const updated = _.keyBy(modificationsByType.update, 'id');

  return _(notes)
    .filter(n => !deleted.has(n.id))
    .map(n => updated[n.id] || n)
    .concat(added)
    .orderBy([n => new Date(n.dateModified)], ['desc'])
    .value();
}

export default {
  namespaced: true,

  state: {
    notes: [],
    modifications: [],
    timer: null,
    lastSync: {
      status: 'normal',
      timestamp: null
    },
    currentNote: null
  },

  getters: {
    localNotes: state => applyModifications(state.notes, state.modifications, m => m.action),

    getNote: (state, getters) => id => _.find(getters.localNotes, { id })
  },

  mutations: {
    [MUT_RESET](state) {
      state.notes = [];
      state.modifications = [];
      state.timer = null;
      state.lastSync.state = 'normal';
      state.lastSync.timestamp = null;
      state.currentNote = null;
    },

    [MUT_SET_NOTES](state, notes) {
      state.notes = notes;
    },

    [MUT_ADD_MODIFICATIONS](state, modifications) {
      state.modifications = _.concat(state.modifications, modifications);
    },

    [MUT_CLEAR_MODIFICATIONS](state) {
      state.modifications = [];
    },

    [MUT_APPLY_UPDATES](state, notes) {
      if (_.isEmpty(notes)) {
        return;
      }

      const existing = new Set(_.map(state.notes, 'id'));

      const modificationType = (n) => {
        if (n.isDeleted) {
          return 'remove';
        }

        return existing.has(n.id) ? 'update' : 'add';
      };

      state.notes = applyModifications(state.notes, notes, modificationType);
    },

    [MUT_SET_TIMER](state, timer) {
      state.timer = timer;
    },

    [MUT_SET_LAST_SYNC_STATUS](state, status) {
      state.lastSync.status = status;
    },

    [MUT_UPDATE_LAST_SYNC_TIMESTAMP](state, notes) {
      state.lastSync.timestamp = _(notes || state.notes)
        .map(n => new Date(n.dateModified).valueOf())
        .max();
    },

    [MUT_SET_CURRENT_NOTE](state, note) {
      state.currentNote = note;
    }
  },

  actions: {
    [GlobalActions.ACT_AFTER_LOGIN]: {
      root: true,
      async handler({ dispatch }) {
        await dispatch(ACT_LOAD_DB);
      }
    },

    [GlobalActions.ACT_AFTER_LOGOUT]: {
      root: true,
      async handler({ commit, dispatch }) {
        dispatch(ACT_CLEAR_SYNC_TIMER);
        commit(MUT_RESET);
      }
    },

    async [ACT_LOAD_DB]({ commit, dispatch }) {
      const userId = AuthApi.userId;
      const notes = await NotesDb.getNotes(userId);
      const modifications = await NotesDb.getModifications(userId);

      commit(MUT_SET_NOTES, notes);
      commit(MUT_UPDATE_LAST_SYNC_TIMESTAMP, notes);
      commit(MUT_CLEAR_MODIFICATIONS);
      commit(MUT_ADD_MODIFICATIONS, modifications);
      commit(MUT_SET_CURRENT_NOTE, null);
      commit(MUT_UPDATE_LAST_SYNC_TIMESTAMP, notes);

      dispatch(ACT_SYNC_TIMER);
    },

    async [ACT_SAVE_DB]({ state }, { notes = true, modifications = true } = {}) {
      const userId = AuthApi.userId;

      if (notes) {
        await NotesDb.setNotes(userId, state.notes);
      }

      if (modifications) {
        await NotesDb.setModifications(userId, state.modifications);
      }
    },

    async [ACT_SYNC]({ state, commit, dispatch }, { incremental = true } = {}) {
      if (!incremental) {
        commit(MUT_SET_NOTES, []);
        commit(MUT_CLEAR_MODIFICATIONS);
        await dispatch(ACT_SAVE_DB);
      }

      const modifications = state.modifications;
      const timestamp = state.lastSync.timestamp;
      const isReadOperation = _.isEmpty(modifications);

      try {
        if (NetworkStatus.isOffline()) {
          throw new Error('App is offline');
        }

        const updates = isReadOperation ?
          await NotesApi.getList(timestamp) :
          await NotesApi.modify(modifications, timestamp);

        commit(MUT_SET_LAST_SYNC_STATUS, 'normal');
        if (_.isEmpty(updates)) {
          return;
        }

        commit(MUT_APPLY_UPDATES, updates);
        commit(MUT_UPDATE_LAST_SYNC_TIMESTAMP, updates);
        commit(MUT_CLEAR_MODIFICATIONS);

        await dispatch(ACT_SAVE_DB);
      } catch (e) {
        commit(MUT_SET_LAST_SYNC_STATUS, isReadOperation ? 'readFail' : 'writeFail');
      }
    },

    [ACT_CLEAR_SYNC_TIMER]({ state, commit }) {
      if (state.timer) {
        clearTimeout(state.timer);
        commit(MUT_SET_TIMER, null);
      }
    },

    async [ACT_SYNC_TIMER]({ state, commit, dispatch }, syncParams) {
      dispatch(ACT_CLEAR_SYNC_TIMER);
      await dispatch(ACT_SYNC, syncParams);

      const interval = TIMER_INTERVAL[state.lastSync.status] || TIMER_INTERVAL.normal;
      const timer = setTimeout(() => dispatch(ACT_SYNC_TIMER, syncParams), interval * 1000);
      commit(MUT_SET_TIMER, timer);
    },

    [ACT_OPEN]({ commit }, { note }) {
      commit(MUT_SET_CURRENT_NOTE, note || {});
    },

    [ACT_CLOSE]({ commit }) {
      commit(MUT_SET_CURRENT_NOTE, null);
    },

    async [ACT_MODIFY]({ commit, dispatch }, modification) {
      const now = new Date();
      const note = { ...modification, dateModified: now.toJSON(), timestamp: now.valueOf() };

      commit(MUT_ADD_MODIFICATIONS, [note]);
      await dispatch(ACT_SAVE_DB, { notes: false });
      await dispatch(ACT_SYNC_TIMER);
    },

    async [ACT_ADD]({ dispatch }, { note }) {
      await dispatch(ACT_MODIFY, { action: 'add', id: new ObjectId().toHexString(), ...note });
    },

    async [ACT_UPDATE]({ dispatch }, { note }) {
      await dispatch(ACT_MODIFY, { action: 'update', ...note });
    },

    async [ACT_REMOVE]({ dispatch }, { note }) {
      await dispatch(ACT_MODIFY, { action: 'remove', id: note.id });
    },

    async [ACT_PUBLISH]({ state, dispatch, commit }) {
      const newNote = await NotesApi.publish(state.currentNote);
      commit(MUT_APPLY_UPDATES, [newNote]);
      commit(MUT_SET_CURRENT_NOTE, newNote);
      await dispatch(ACT_SAVE_DB);
    },

    async [ACT_UNPUBLISH]({ state, dispatch, commit }) {
      const newNote = await NotesApi.unpublish(state.currentNote);
      commit(MUT_APPLY_UPDATES, [newNote]);
      commit(MUT_SET_CURRENT_NOTE, newNote);
      await dispatch(ACT_SAVE_DB);
    }
  }
};
