export const ACT_INIT = 'init';
export const ACT_AFTER_LOGOUT = 'afterLogout';
export const ACT_AFTER_LOGIN = 'afterLogin';
