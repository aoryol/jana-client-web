import AuthApi from '../api/auth-api';

import * as GlobalActions from './global-actions';

const MUT_SET_IS_AUTHENTICATED = 'setIsAuthenticated';

export default {
  namespaced: true,

  state: {
    isAuthenticated: null
  },

  getters: {
    needAuth: state => state.isAuthenticated === false,
    notNeedAuth: state => state.isAuthenticated === true
  },

  mutations: {
    [MUT_SET_IS_AUTHENTICATED](state, value) {
      state.isAuthenticated = value;
    }
  },

  actions: {
    [GlobalActions.ACT_INIT]: {
      root: true,
      async handler({ commit, dispatch }) {
        const isAuthenticated = await AuthApi.check();
        commit(MUT_SET_IS_AUTHENTICATED, isAuthenticated);

        if (isAuthenticated) {
          await dispatch(GlobalActions.ACT_AFTER_LOGIN, null, { root: true });
        } else {
          await dispatch(GlobalActions.ACT_AFTER_LOGOUT, null, { root: true });
        }
      }
    },

    async login({ commit, dispatch }, { token }) {
      await AuthApi.login(token);
      commit(MUT_SET_IS_AUTHENTICATED, true);
      await dispatch(GlobalActions.ACT_AFTER_LOGIN, null, { root: true });
    },

    async logout({ commit, dispatch }) {
      AuthApi.logout();
      commit(MUT_SET_IS_AUTHENTICATED, false);
      await dispatch(GlobalActions.ACT_AFTER_LOGOUT, null, { root: true });
    }
  }
};
