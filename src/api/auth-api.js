import axios from 'axios';
import jwtDecode from 'jwt-decode';
import debug from 'debug';
import UserAgentParser from 'ua-parser-js';

import NetworkStatus from '../services/network-status';

const logger = debug('jana:auth-api');

const tokenStorage = {
  KEY: 'jana.auth',

  get() {
    return localStorage.getItem(this.KEY);
  },

  set(val) {
    if (val) {
      localStorage.setItem(this.KEY, val);
    } else {
      localStorage.removeItem(this.KEY);
    }
  },

  decode() {
    const token = this.get();
    return token ? jwtDecode(token) : null;
  }
};

const userClientGenerator = {
  get() {
    return {
      id: this.id(),
      name: this.name()
    };
  },

  id() {
    const storageKey = 'jana.client_id';
    let id = +localStorage.getItem(storageKey);
    if (id > 0) {
      return id;
    }

    const d0 = new Date('2018-01-01');
    const num1 = (new Date() - d0) / (60 * 60 * 1000);
    const num2 = Math.floor(Math.random() * 256);
    id = (num1 << 8) | num2; /* eslint no-bitwise: "off" */

    localStorage.setItem(storageKey, id);
    return id;
  },

  name() {
    const ua = new UserAgentParser(navigator.userAgent).getResult();

    const browser = `${ua.browser.name} ${ua.browser.major}`;
    const os = ua.os.name;
    const device = ua.device.model ? `${ua.device.vendor} ${ua.device.model}` : null;

    return `${browser} (${device || os})`;
  }
};

const serverApi = {
  setAuth() {
    const token = tokenStorage.get();
    axios.defaults.headers.common.Authorization = token ? `Bearer ${token}` : null;
  },

  async refreshToken() {
    try {
      const client = userClientGenerator.get();
      const token = (await axios.post('auth/refresh', { client })).data.token;
      tokenStorage.set(token);
      this.setAuth();
    } catch (e) {
      logger(`Couldn't refresh token: ${e.message}`);
    }
  }
};

const timer = {
  INTERVAL: 60 * 60 * 1000,

  set() {
    this.remove();
    this._timer = setTimeout(this.handler.bind(this), this.INTERVAL);
  },

  remove() {
    if (this._timer) {
      clearTimeout(this._timer);
      this._timer = null;
    }
  },

  async handler() {
    if (!NetworkStatus.isOffline()) {
      await serverApi.refreshToken();
    }

    this.set();
  }
};

export default {
  async check() {
    const jwt = tokenStorage.decode();
    if (!jwt) {
      return false;
    }

    serverApi.setAuth();

    if (NetworkStatus.isOffline()) {
      timer.set();
      return true;
    }

    const nowSeconds = Date.now() / 1000;
    const expiresInMinutes = (jwt.exp - nowSeconds) / 60;

    if (expiresInMinutes < 1) {
      this.logout();
      return false;
    }

    if (expiresInMinutes < 30) {
      await timer.handler();
    } else {
      timer.set();
    }

    return true;
  },

  async login(token) {
    tokenStorage.set(token);
    serverApi.setAuth();
    await timer.handler();
  },

  logout() {
    timer.remove();
    tokenStorage.set(null);
    serverApi.setAuth();
  },

  get userId() {
    return tokenStorage.decode().id || '';
  }
};
