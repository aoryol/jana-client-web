import axios from 'axios';

export default {
  getList(timestamp) {
    return axios
      .get('api/notes', { params: { timestamp } })
      .then(r => r.data);
  },

  get(name) {
    return axios.get(`public-api/notes/${name}`).then(r => r.data);
  },

  modify(notes, timestamp) {
    return axios
      .patch('api/notes', notes, { params: { timestamp } })
      .then(r => r.data);
  },

  publish(note) {
    return axios
      .post(`api/notes/${note.id}/public-name`, note)
      .then(r => r.data);
  },

  unpublish(note) {
    return axios
      .delete(`api/notes/${note.id}/public-name`)
      .then(r => r.data);
  }
};
