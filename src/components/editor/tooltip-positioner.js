import { Plugin } from 'prosemirror-state';

const TOOLTIP_CSS = {
  horizontalLeft: 'editor-tooltip_h-pos_left',
  horizontalCenter: 'editor-tooltip_h-pos_center',
  horizontalRight: 'editor-tooltip_h-pos_right',
  verticalTop: 'editor-tooltip_v-pos_top',
  verticalCenter: 'editor-tooltip_v-pos_center',
  verticalBottom: 'editor-tooltip_v-pos_bottom'
};

const OFFSET = {
  horizontal: 6,
  vertical: 20
};

class TooltipPositioner {
  constructor(view, tooltip) {
    this.view = view;
    this.tooltip = tooltip;

    this.tooltip.style.display = 'block';
    const tooltipSize = this.tooltip.getBoundingClientRect();
    this.tooltipSize = {
      width: tooltipSize.width,
      halfWidth: tooltipSize.width / 2,
      height: tooltipSize.height
    };

    this.update(view, null);

    this.onBlur = this.onBlur.bind(this);
    this.view.dom.addEventListener('blur', this.onBlur);
  }

  onBlur() {
    setTimeout(() => {
      if (!this.view.hasFocus()) this.tooltip.style.display = 'none';
    }, 200);
  }

  destroy() {
    this.view.dom.removeEventListener('blur', this.onBlur);
  }

  update(view, lastState) {
    const { state } = view;

    if (lastState && lastState.doc.eq(state.doc) && lastState.selection.eq(state.selection)) {
      return;
    }

    const tooltipCls = this.tooltip.classList;
    const tooltipStyle = this.tooltip.style;

    if (state.selection.empty) {
      tooltipStyle.display = 'none';
      return;
    }

    const { from, to } = state.selection;
    const fromCoords = view.coordsAtPos(from);
    const toCoords = view.coordsAtPos(to);

    const calcSelPos = (startProp, endProp) => {
      const start = Math.min(fromCoords[startProp], toCoords[startProp]);
      const end = Math.max(fromCoords[endProp], toCoords[endProp]);
      return { start, end, center: (start + end) / 2 };
    };

    const selPos = {
      h: calcSelPos('left', 'right'),
      v: calcSelPos('top', 'bottom')
    };

    const parentSize = this.tooltip.parentElement.getBoundingClientRect();
    tooltipStyle.display = 'block';

    // horizontal position
    tooltipCls.remove(
      TOOLTIP_CSS.horizontalLeft,
      TOOLTIP_CSS.horizontalCenter,
      TOOLTIP_CSS.horizontalRight
    );

    if (selPos.h.center < this.tooltipSize.halfWidth) {
      tooltipCls.add(TOOLTIP_CSS.horizontalLeft);
      tooltipStyle.left = `${OFFSET.horizontal - parentSize.left}px`;
    } else if ((selPos.h.center + this.tooltipSize.halfWidth) >= document.body.clientWidth) {
      tooltipCls.add(TOOLTIP_CSS.horizontalRight);

      const value = document.body.clientWidth -
        (this.tooltipSize.width + parentSize.left) -
        OFFSET.horizontal;

      tooltipStyle.left = `${value}px`;
    } else {
      tooltipCls.add(TOOLTIP_CSS.horizontalCenter);
      tooltipStyle.left = `${selPos.h.center - this.tooltipSize.halfWidth - parentSize.left}px`;
    }

    // vertical position
    tooltipCls.remove(
      TOOLTIP_CSS.verticalTop,
      TOOLTIP_CSS.verticalCenter,
      TOOLTIP_CSS.verticalBottom
    );

    if ((selPos.v.start - OFFSET.vertical) > this.tooltipSize.height) {
      tooltipCls.add(TOOLTIP_CSS.verticalTop);
      const value = selPos.v.start - this.tooltipSize.height - parentSize.top - OFFSET.vertical;
      tooltipStyle.top = `${value}px`;
    } else {
      tooltipCls.add(TOOLTIP_CSS.verticalCenter);
      tooltipStyle.top = `${selPos.v.start - parentSize.top}px`;
    }
  }
}

export default function (tooltip) {
  return new Plugin({
    view(editor) {
      return new TooltipPositioner(editor, tooltip);
    }
  });
}
