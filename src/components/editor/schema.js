import { Schema, DOMSerializer } from 'prosemirror-model';

const schema = new Schema({
  nodes: {
    doc: {
      content: 'block*'
    },
    paragraph: {
      attrs: {
        align: { default: 'left' }
      },
      content: 'inline*',
      group: 'block',
      parseDOM: [{ tag: 'p' }],
      toDOM(node) {
        return ['p', { style: `text-align: ${node.attrs.align}` }, 0];
      }
    },
    heading: {
      attrs: {
        level: { default: 1 },
        align: { default: 'left' }
      },
      content: 'inline*',
      group: 'block',
      defining: true,
      parseDOM: [
        { tag: 'h1', attrs: { level: 1 } },
        { tag: 'h2', attrs: { level: 2 } },
        { tag: 'h3', attrs: { level: 3 } },
        { tag: 'h4', attrs: { level: 4 } },
        { tag: 'h5', attrs: { level: 5 } },
        { tag: 'h6', attrs: { level: 6 } }
      ],
      toDOM(node) {
        return [`h${node.attrs.level}`, { style: `text-align: ${node.attrs.align}` }, 0];
      }
    },
    text: {
      group: 'inline'
    },
    hardBreak: {
      inline: true,
      group: 'inline',
      selectable: false,
      parseDOM: [{ tag: 'br' }],
      toDOM() { return ['br']; }
    }
  },
  marks: {
    italic: {
      parseDOM: [{ tag: 'i' }, { tag: 'em' }, { style: 'font-style=italic' }],
      toDOM() { return ['i', 0]; }
    },
    bold: {
      parseDOM: [{ tag: 'b' }, { tag: 'strong' }, { style: 'font-style=bold' }],
      toDOM() { return ['b', 0]; }
    }
  }
});

schema.serializer = DOMSerializer.fromSchema(schema);

export default schema;
