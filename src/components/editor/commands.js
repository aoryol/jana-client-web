import { setBlockType, toggleMark } from 'prosemirror-commands';
import { undo, redo } from 'prosemirror-history';

import schema from './schema';


export { undo, redo };

export const bold = toggleMark(schema.marks.bold);
export const italic = toggleMark(schema.marks.italic);

export const hardBreak = (state, dispatch) => {
  const createHardBreak = schema.nodes.hardBreak.create();
  const transaction = state.tr.replaceSelectionWith(createHardBreak).scrollIntoView();
  dispatch(transaction);
  return true;
};

export const normalParagraph = setBlockType(schema.nodes.paragraph);


function heading(level) {
  return setBlockType(schema.nodes.heading, { level });
}

export const heading1 = heading(1);
export const heading2 = heading(2);
export const heading3 = heading(3);
export const heading4 = heading(4);
export const heading5 = heading(5);
export const heading6 = heading(6);


function align(alignment) {
  return (state, dispatch) => {
    const { from, to } = state.selection;
    let tr = state.tr;

    state.doc.nodesBetween(from, to, (node, pos) => {
      if (node.attrs.align) {
        tr = tr.setNodeMarkup(pos, null, { ...node.attrs, align: alignment });
      }
    });

    dispatch(tr.scrollIntoView());
    return true;
  };
}

export const alignLeft = align('left');
export const alignCenter = align('center');
export const alignRight = align('right');
