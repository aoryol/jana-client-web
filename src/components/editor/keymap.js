import { keymap } from 'prosemirror-keymap';
import { baseKeymap } from 'prosemirror-commands';

import * as commands from './commands';


export default keymap({
  ...baseKeymap,
  'Mod-z': commands.undo,
  'Shift-Mod-z': commands.redo,
  'Mod-b': commands.bold,
  'Mod-i': commands.italic,
  'Shift-Enter': commands.hardBreak,
  'Shift-Ctrl-0': commands.normalParagraph,
  'Shift-Ctrl-1': commands.heading1,
  'Shift-Ctrl-2': commands.heading2,
  'Shift-Ctrl-3': commands.heading3,
  'Shift-Ctrl-4': commands.heading4,
  'Shift-Ctrl-5': commands.heading5,
  'Shift-Ctrl-6': commands.heading6,
  'Shift-Ctrl-,': commands.alignLeft,
  'Shift-Ctrl-.': commands.alignRight,
  'Shift-Ctrl--': commands.alignCenter
});
