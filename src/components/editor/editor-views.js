import { EditorState } from 'prosemirror-state';
import { EditorView as ProseMirrorEditor } from 'prosemirror-view';
import { history } from 'prosemirror-history';

import schema from './schema';
import keymap from './keymap';
import tooltipPositioner from './tooltip-positioner';
import * as commands from './commands';

export class EditorView {
  constructor({
    container, tooltip, content, onChange
  }) {
    this.tooltipPositioner = tooltipPositioner(tooltip);
    this._editor = new ProseMirrorEditor(container, {
      state: createState(content, [this.tooltipPositioner]),
      dispatchTransaction: (tx) => {
        const newState = this._editor.state.apply(tx);
        this._editor.updateState(newState);
        if (onChange) onChange();
      }
    });
  }

  destroy() {
    this._editor.destroy();
  }

  get content() {
    return saveDoc(this._editor.state.doc);
  }

  set content(value) {
    if (this.content === value) return;
    this._editor.updateState(createState(value));
  }

  runCommand(commandName) {
    const cmd = commands[commandName];
    if (cmd) {
      cmd(this._editor.state, this._editor.dispatch, this._editor);
      this._editor.focus();
    }
  }
}

export class ViewerView {
  constructor({ container, content }) {
    this.container = container;
    this.doc = parseDoc(content);
    this.render();
  }

  render() {
    while (this.container.firstChild) {
      this.container.removeChild(this.container.firstChild);
    }

    const dom = schema.serializer.serializeFragment(this.doc.content);
    this.container.appendChild(dom);
  }

  get content() {
    return saveDoc(this.doc);
  }

  set content(value) {
    if (this.content === value) return;
    this.doc = parseDoc(value);
    this.render();
  }
}

function createState(content, additionalPlugins = []) {
  const doc = parseDoc(content);
  const plugins = [history(), keymap, ...additionalPlugins];
  return EditorState.create({ schema, doc, plugins });
}

function parseDoc(content) {
  return schema.nodeFromJSON({
    type: 'doc',
    content: [{
      type: 'paragraph'
    }],
    ...safeParse(content)
  });
}

function saveDoc(doc) {
  return JSON.stringify(doc.toJSON());
}

function safeParse(json) {
  try {
    return JSON.parse(json);
  } catch (e) {
    return {};
  }
}
