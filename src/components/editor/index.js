import './styles.scss';
import { EditorView } from './editor-views';

window.editor = new EditorView({
  container: document.querySelector('#editor-container')
});
