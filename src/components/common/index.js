import Vue from 'vue';

import MdIcon from './md-icon';
import SocIcon from './soc-icon';
import ZeroButton from './zero-button';

Vue.component(MdIcon.name, MdIcon);
Vue.component(SocIcon.name, SocIcon);
Vue.component(ZeroButton.name, ZeroButton);
