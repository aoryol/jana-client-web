module.exports = {
  moduleNameMapper: {
    '^~/(.*)$': '<rootDir>/src/$1'
  },
  collectCoverageFrom: [
    'src/**/*.js',
    'src/**/*.vue'
  ],
  setupFiles: ['jest-localstorage-mock'],
  testRegex: '/test/.*\\.test\\.js$',
  testURL: 'http://localhost'
};
