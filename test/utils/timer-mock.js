const originalFn = {
  now: Date.now
};

const state = {
  now: Date.now()
};

export function mock() {
  state.now = originalFn.now();

  Date.now = jest.fn(() => state.now);
  jest.useFakeTimers();
}

export function restore() {
  Date.now = originalFn.now;
  jest.useRealTimers();
}

export function advance(ms) {
  state.now += ms;
  jest.advanceTimersByTime(ms);
  return Promise.resolve(); // wait for promise to complete
}
