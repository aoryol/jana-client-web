import Store from '~/store';

import NotesApi from '~/api/notes-api';

describe('app store', () => {
  let getListApi;

  beforeAll(() => {
    getListApi = jest.spyOn(NotesApi, 'getList')
      .mockImplementation(() => [{ id: 'new note', title: 'note' }]);
  });

  afterAll(() => {
    getListApi.mockRestore();
  });

  test('auth/logout resets notes list', async () => {
    await Store.dispatch('notes/sync');
    expect(Store.state.notes.notes.length).toBe(1);

    await Store.dispatch('auth/logout');
    expect(Store.state.notes.notes.length).toBe(0);
  });
});
