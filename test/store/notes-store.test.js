import NotesStore from '~/store/notes-store';
import NotesApi from '~/api/notes-api';

describe('notes store', () => {
  describe('#applyUpdates', () => {
    const applyUpdates = NotesStore.mutations.applyUpdates;

    test('It does nothing if no notes are provided', () => {
      const state = {};
      applyUpdates(state, []);
      expect(state).toEqual({});
    });

    test('It correctly merges notes', () => {
      const state = {
        notes: [
          {
            id: 'Should be deleted',
            title: 'Old Note',
            dateModified: new Date('2018-01-01 08:02')
          },
          {
            id: 'Should not be changed',
            title: 'Old Note',
            dateModified: new Date('2018-01-01 08:01')
          },
          {
            id: 'Should be updated',
            title: 'Old Note',
            dateModified: new Date('2018-01-01 08:00')
          }
        ]
      };

      const changedNotes = [
        {
          id: 'Should be added',
          title: 'New Note',
          dateModified: new Date('2018-01-01 09:00')
        },
        {
          id: 'Should be deleted',
          isDeleted: true,
          dateModified: new Date('2018-01-01 09:01')
        },
        {
          id: 'Should be updated',
          isDeleted: false, // explicit
          title: 'New Note',
          dateModified: new Date('2018-01-01 09:02')
        }
      ];

      applyUpdates(state, changedNotes);

      expect(state.notes).toEqual([
        {
          id: 'Should be updated',
          isDeleted: false,
          title: 'New Note',
          dateModified: new Date('2018-01-01 09:02')
        },
        {
          id: 'Should be added',
          title: 'New Note',
          dateModified: new Date('2018-01-01 09:00')
        },
        {
          id: 'Should not be changed',
          title: 'Old Note',
          dateModified: new Date('2018-01-01 08:01')
        }
      ]);
    });
  });

  describe('#updateLastSyncTimestamp', () => {
    const updateLastSyncTimestamp = NotesStore.mutations.updateLastSyncTimestamp;

    test('It correctly updates last sync timestamp', () => {
      const state = { lastSync: {} };

      const changedNotes = [
        { dateModified: new Date('2018-01-01 09:01') },
        { dateModified: new Date('2018-01-01 09:02') },
        { dateModified: new Date('2018-01-01 09:00') }
      ];

      updateLastSyncTimestamp(state, changedNotes);

      expect(state.lastSync.timestamp).toBe(new Date('2018-01-01 09:02').valueOf());
    });
  });

  describe('#sync', () => {
    const sync = NotesStore.actions.sync;

    const commit = jest.fn();
    const dispatch = jest.fn();

    let getListApi;
    let modifyApi;

    beforeAll(() => {
      getListApi = jest.spyOn(NotesApi, 'getList').mockImplementation(() => ['note']);
      modifyApi = jest.spyOn(NotesApi, 'modify').mockImplementation(() => ['note']);
    });

    afterAll(() => {
      getListApi.mockRestore();
      modifyApi.mockRestore();
    });

    afterEach(() => {
      commit.mockClear();
      dispatch.mockClear();

      getListApi.mockClear();
      modifyApi.mockClear();
    });

    test('It merges notes with data returned from getList API', async () => {
      const state = {
        modifications: [],
        lastSync: { timestamp: 'sync timestamp' }
      };

      await sync({ state, commit, dispatch });

      expect(getListApi).toBeCalled();
      expect(modifyApi).not.toBeCalled();

      expect(commit).nthCalledWith(1, 'setLastSyncStatus', 'normal');
      expect(commit).nthCalledWith(2, 'applyUpdates', ['note']);

      const getListApiArgs = getListApi.mock.calls[0];
      expect(getListApiArgs[0]).toBe('sync timestamp');
    });

    test('It merges notes with data returned from modify API', async () => {
      const state = {
        modifications: ['new note'],
        lastSync: { timestamp: 'sync timestamp' }
      };

      await sync({ state, commit, dispatch });

      expect(getListApi).not.toBeCalled();
      expect(modifyApi).toBeCalledWith(['new note'], 'sync timestamp');

      expect(commit).nthCalledWith(1, 'setLastSyncStatus', 'normal');
      expect(commit).nthCalledWith(2, 'applyUpdates', ['note']);
    });
  });
});
