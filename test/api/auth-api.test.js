jest.mock('jwt-decode', () => token => JSON.parse(token));
jest.mock('axios', () => ({
  defaults: {
    headers: {
      common: {
        Authorization: null
      }
    }
  },
  post: jest.fn(() => Promise.resolve({
    data: {
      token: JSON.stringify({ exp: Date.now() })
    }
  }))
}));

import axios from 'axios';

import AuthApi from '~/api/auth-api';

import * as TimerMock from '../utils/timer-mock';

describe('AuthApi', () => {
  beforeEach(() => {
    TimerMock.mock();
    AuthApi.logout();
    localStorage.clear();
    axios.defaults.headers.common.Authorization = null;
  });

  afterEach(() => {
    TimerMock.restore();
  });

  const setToken = token => localStorage.setItem('jana.auth', JSON.stringify(token));
  const getToken = () => JSON.parse(localStorage.getItem('jana.auth'));

  describe('#check', () => {
    describe('If there is no token', () => {
      test('It returns false', async () => {
        const result = await AuthApi.check();
        expect(result).toBe(false);
      });
    });

    describe('If token is expired', () => {
      beforeEach(() => {
        setToken({
          exp: (Date.now() / 1000) - 1
        });
      });

      test('It returns false and clears token and storage and auth params', async () => {
        const result = await AuthApi.check();

        expect(result).toBe(false);

        expect(getToken()).toBe(null);
        expect(axios.defaults.headers.common.Authorization).toBe(null);
      });
    });

    describe('If there is valid token with short expiration', async () => {
      beforeEach(() => {
        setToken({
          exp: (Date.now() / 1000) + (10 * 60)
        });
      });

      test('It returns true and refreshes token', async () => {
        const initExpiration = getToken().exp;
        const result = await AuthApi.check();

        expect(result).toBe(true);
        expect(getToken().exp).toBeGreaterThan(initExpiration);
      });

      test('It refreshes token again after 1 hour', async () => {
        await AuthApi.check();
        const expiration = getToken().exp;
        await TimerMock.advance(60 * 60 * 1000);

        expect(getToken().exp).toBeGreaterThan(expiration);
      });
    });

    describe('If there is valid token with big expiration', async () => {
      beforeEach(() => {
        setToken({
          exp: (Date.now() / 1000) + (40 * 60)
        });
      });

      test('It returns true and refreshes token after 1 hour', async () => {
        const result = await AuthApi.check();
        expect(result).toBe(true);

        const expiration = getToken().exp;
        await TimerMock.advance(60 * 60 * 1000);

        expect(getToken().exp).toBeGreaterThan(expiration);
      });
    });
  });

  describe('#logout', () => {
    beforeEach(() => {
      axios.defaults.headers.common.Authorization = 'auth token';
    });

    test('It clears auth token for axios', () => {
      AuthApi.logout();
      expect(axios.defaults.headers.common.Authorization).toBe(null);
    });
  });
});
